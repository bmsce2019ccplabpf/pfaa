#include <stdio.h>
int swap (int *a, int *b);

int
main () 
{
  
int a, b;
  
printf ("enter the values of a and b \n");
  
scanf ("%d%d", &a, &b);
  
swap (&a, &b);
  
printf ("enter values of a and  b after swapping are");
  
printf (" %d %d ", a, b);
  
return 0;

}


int
swap (int *a, int *b) 
{
  
int temp;
  
temp = *a;
  
*a = *b;
  
*b = temp;
  
return 0;

}
